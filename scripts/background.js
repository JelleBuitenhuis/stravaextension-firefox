chrome.scripting.registerContentScripts([{
    runAt: "document_end",
    js: [
        "scripts/elementCreateHelpers.js",
        "scripts/sortHelpers.js",
        "scripts/util.js",
        "scripts/injected.js"
    ],
    id: "injected",
    matches: ["https://*.strava.com/*"],
}]);

chrome.webNavigation.onHistoryStateUpdated.addListener(function (details) {
    chrome.scripting.executeScript({
        target: {tabId: details.tabId},
        files: [
            "scripts/elementCreateHelpers.js",
            "scripts/sortHelpers.js",
            "scripts/util.js",
            "scripts/injected.js"
        ],
    });
}, {url: [{hostSuffix: '.strava.com'}]});

{
    var equalize = false;
    var originalSort = false;
    var sortAmount = 0;
    var ascending = 1;
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    if (window.location.href.includes('strava') && !window.localStorage.getItem("flybyIds")) {
        window.localStorage.setItem("flybyIds", "[]")
    }

    function hmsToSecondsOnly(str) {
        let p = str.split(':'),
            s = 0, m = 1;

        while (p.length > 0) {
            s += m * parseInt(p.pop(), 10);
            m *= 60;
        }

        return s;
    }

    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }


    function checkIfBikePage() {
        let bikePage;
        try {
            let activityData = JSON.parse(document.querySelector('[data-react-class="ActivityTagging"]').getAttribute('data-react-props'))
            bikePage = activityData["activityType"] === "ride"
        } catch (e) {
            bikePage = document.querySelector('[class="title"]').innerText.includes('Ride')
        }
        return bikePage;
    }

    // TODO: Use MutationObserver?
    function waitFor(check, callback) {
        if (!check()) {
            setTimeout(() => waitFor(check, callback), 100)
        } else {
            callback();
        }

    }
}
